<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
    
    <style type="text/css">
        table, tr, td, th{
            border: 1px solid black;
            border-collapse: collapse;
        }
    </style>
</head>

<body>

    <h1>UPDATE CLASS</h1>
    <table>
    	<form action="../../class/update-process" method="POST">
    	<tr>
    		<th>ID: </th>
    		<td><input type="text" name="id-class" readonly value="<?= $item->idClass ?>"></td>
    	</tr>
    	<tr>
    		<th>NAME: </th>
            <td><input type="text" name="name-class" value="<?= $item->nameClass ?>"></td>
    	</tr>
    	<tr>
    		<td colspan="2"><button>OK</button></td>
    	</tr>
    	</form>
    </table>
   
        
    
</body>

</html>



