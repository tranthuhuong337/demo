<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>

    <style type="text/css">
        table, tr, td, th{
            border: 1px solid black;
            border-collapse: collapse;
        }
    </style>
</head>

<body>
    <h1>Danh Sách Lớp</h1>
    <table>
        <tr>
            <th>Mã</th>
            <th>Tên</th>
            <th>Update</th>
            <th>Delete</th>
        </tr>
        <?php foreach ($listClass as $class) : ?>
            <tr>
                <td><?= $class->idClass ?></td>
                <td><?= $class->nameClass ?></td>
                <td><a href="class/update/<?= $class->idClass ?>">Sửa</a></td>
                <td><a href="class/delete/<?= $class->idClass ?>" onclick="return confirm('Bạn có chắc muốn xoá lớp này không?')">Xoá</a></td>
            </tr>
        <?php endforeach ?>
    </table>
</body>

</html>



