<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>

    <style type="text/css">
        table, tr, td, th{
            border: 1px solid black;
            border-collapse: collapse;
        }
    </style>
</head>

<body>
    <h1>Danh Sách Sinh Viên</h1>
    <table>
        <tr>
            <th>ID_STUDENT</th>
            <th>NAME</th>
            <th>GENDER</th>
            <th>DATE_BIRTH</th>
            <th>NAME_CLASS</th>
            <th>Update</th>

        </tr>
        <?php foreach ($listStudent as $student) : ?>
            <tr>
                <td><?= $student->idStudent ?></td>
                <!-- nối chuỗi -->
                <td><?= $student->firstName." ".$student->lastName ?></td>

                <!-- điều kiện -->
                <td><?= $student->gender == 1 ? "Nam" : "Nữ" ?></td>
                <td><?= $student->dateBirth ?></td>
                <td><?= $student->class ?></td>
  
                <td><a href="student/update/<?= $student->idStudent ?>">Sửa</a></td>
            
            </tr>
        <?php endforeach ?>
    </table>
</body>

</html>



