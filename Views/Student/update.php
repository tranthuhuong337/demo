<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
    
    <style type="text/css">
        table, tr, td, th{
            border: 1px solid black;
            border-collapse: collapse;
        }
    </style>
</head>

<body>

    <h1>UPDATE STUDENT</h1>
    <table>
    	<form action="../../Student/update-process" method="POST">
    	<tr>
    		<th>ID_STUDEN: </th>
    		<td><input type="text" name="id-student" readonly value="<?= $item->idStudent ?>"></td>
            <!--readonly:ko đc điền-->
    	</tr>
    	<tr>
    		<th>FIRST_NAME: </th>
    		<td><input type="text" name="first-name" value="<?= $item->firstName ?>"></td>
    	</tr>
        <tr>
            <th>LAST_NAME: </th>
            <td><input type="text" name="last-name" value="<?= $item->lastName ?>"></td>
        </tr>
        <tr>
            <th>GENDER: </th>
            <td>
                <input type="radio" name="gender" value="1" <?= $item->gender == 1 ? 'checked' : '' ?> >Nam
                <input type="radio" name="gender" value="0" <?= $item->gender == 0 ? 'checked' : '' ?> >Nữ
            </td>
        </tr>
        <tr>
            <th>DATE_BIRTH: </th>
            <td><input type="date" name="date-birth" value="<?= $item->dateBirth ?>"></td>
        </tr>
        <tr>
            <th>NAME_CLASS: </th>
            <td>
                <select name="class">
                    <?php foreach ($listClass as $class) : ?>
                    <option value="<?= $class->idClass ?>" <?= $item->class->idClass == $class->idClass ? 'selected' : '' ?>>
                        <?= $class->nameClass ?> <!--hiển thị tên lớp-->
                    </option>
                    <?php endforeach ?>
                </select>
            </td>
        </tr>
    	<tr>
    		<td colspan="2"><button>OK</button></td>
    	</tr>
    	</form>
    </table>
   
        
    
</body>

</html>
