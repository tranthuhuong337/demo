<?php
//import file ClassModel vào file ClassController 1 lần
require_once 'Model/ClassModel.php';
class ClassController
{
    static function viewClass()
    {
        // Lấy dữ liệu
        $class = new ClassModel();
        $listClass = $class->getAll();
        // Hiển thị dữ liệu
        require_once 'Views/Class/view-all.php';
    }

    static function viewUpdate()
    {
        // Lấy id
        $id = $_GET["id"];
        $class = new ClassModel();
        $item = $class->getById($id);
        // Hiển thị form cập nhật
        require_once "Views/Class/update.php";
    }

    static function updateProcess()
    {
    	//nhập dữ liệu từ form

    	$class = new ClassModel();
    	$class->idClass = $_POST["id-class"];
    	$class->nameClass = $_POST["name-class"];
    	$class->update();
    	header("Location: ../class");

    }

    static function delete()
    {
        $class = new ClassModel();
        $class->deleteById($_GET["id"]);
        header("Location: ../class");
    }
}