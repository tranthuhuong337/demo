<?php
require_once 'Model/StudentModel.php';
class StudentController
{
    static function viewStudent()
    {
        // Lấy dữ liệu
        $student = new StudentModel();
        $listStudent = $student->getAll();
        // Hiển thị dữ liệu
        require_once 'Views/Student/view-all.php';
    }

    static function viewUpdate()
    {
    // Lấy id
        $id = $_GET["id"];
        $student = new StudentModel();
        $item = $student->getById($id);
        $class = new ClassModel();
        $listClass = $class->getAll();
    // Hiển thị form cập nhật
        require_once "Views/Student/update.php";
    }

    static function updateProcess()
    {
    //nhập dữ liệu từ form

        $student = new StudentModel();
        $student->idStudent = $_POST["id-student"];
        $student->firstName = $_POST["first-name"];
        $student->lastName = $_POST["last-name"];
        $student->gender = $_POST["gender"];
        $student->dateBirth = $_POST["date-birth"];
        $student->class = $_POST["class"];
        $student->update();
        header("Location: ../student");

    }

}