<?php
// Nhúng mọi thứ thì phải ở trên đầu nó
require_once "Controller/HomeController.php";
require_once "Controller/ClassController.php";
require_once "Controller/StudentController.php";
// Xử lý liên quan tới đường dẫn
$controller = isset($_GET["controller"]) ? $_GET["controller"] : "";
$action = isset($_GET["action"]) ? $_GET["action"] : "";

switch ($controller) {
    case '':
        # Hiển thị menu
        // Nhúng controller  
        HomeController::viewMenu();
        break;
    case 'class':
        switch ($action) {
            case '':
                ClassController::viewClass();
                break;
            case 'update':
                ClassController::viewUpdate();
                break;
            case 'update-process':
                ClassController::updateProcess();
                break;
            case 'delete':
                ClassController::delete();
                break;
            default:
                echo "Không có chức năng này!";
                break;
        }
        break;
    case 'student':
        switch ($action) {
            case '':
                StudentController::viewStudent();
                break;
            case 'update':
                StudentController::viewUpdate();
                break;
            case 'update-process':
                StudentController::updateProcess();
                break;

            default:
                echo "Không có chức năng này!";
                break;
        }
        break;
    default:
        echo "Không có trang này!";
        break;
}