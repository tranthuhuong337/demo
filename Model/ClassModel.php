<?php
require_once 'Model/DatabaseModel.php';
class ClassModel extends DatabaseModel
{
    // properties
    public $idClass;
    public $nameClass;

    // methods
    // Lấy tất cả bản ghi
    public function getAll()
    {
        // mở kết nối
        $connect = $this->open();
        $sql = "SELECT * FROM `CLASS`";
        $result = mysqli_query($connect, $sql);
        // đóng kết nối
        $this->close($connect);
        $array = [];
        foreach ($result as $each) {
            $class = new ClassModel();
            $class->idClass = $each["ID_CLASS"];
            $class->nameClass = $each["NAME_CLASS"];
            array_push($array, $class);
        }
        return $array;
    }
    public function getById($id)
    {
        // mở kết nối
        $connect = $this->open();
        $sql = "SELECT * FROM `CLASS` WHERE ID_CLASS=$id";
        $result = mysqli_query($connect, $sql);
        // đóng kết nối
        $this->close($connect);
        $item = mysqli_fetch_assoc($result);
        $class = new ClassModel();
        $class->idClass = $item["ID_CLASS"];
        $class->nameClass = $item["NAME_CLASS"];
        return $class;
    }

    public function update()
    {

        //mở kết nối
        $connect = $this->open();
        $sql = "UPDATE CLASS SET NAME_CLASS = '$this->nameClass' WHERE ID_CLASS= $this->idClass";
        mysqli_query($connect, $sql);
        
        // //đóng kết nối
        $this->close($connect);
        
    }

        public function deleteById($id)
    {

        //mở kết nối
        $connect = $this->open();

        //C1:PHP1
        // $sqlStudent = "DELETE FROM STUDENT WHERE ID_CLASS =$id";
        // mysqli_query($connect, $sqlStudent);
        // $sqlClass = "DELETE FROM CLASS WHERE ID_CLASS= $id";
        // mysqli_query($connect, $sqlClass);
        
        //C2:PHP2
        /*
        DELIMITER
        CREATE PROCEDURE DELETE_CLASS
        ( 
            id INT 
        ) 

        BEGIN 
            DELETE FROM student1 WHERE ID_CLASS = id; 
            DELETE FROM class WHERE ID_CLASS = id; 

        END; $$
         */
        $sql = "CALL DELETE_CLASS($id)";
        mysqli_query($connect, $sql);

        // //đóng kết nối
        $this->close($connect);
        
    }
}