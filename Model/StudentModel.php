<?php
//nhúng
require_once 'Model/DatabaseModel.php';
class StudentModel extends DatabaseModel
{
    // properties
    public $firstName;
    public $lastName;
    public $gender;
    public $dateBirth;
    public $class;

    // methods
    // Lấy tất cả bản ghi
    public function getAll()
    {
        // mở kết nối
        $connect = $this->open();
        $sql = "SELECT * FROM `STUDENT1`";
        $result = mysqli_query($connect, $sql);
        // đóng kết nối
        $this->close($connect);
        $array = [];
        foreach ($result as $each) {
            $student = new StudentModel();
            $student->idStudent = $each["ID_STUDENT"];
            $student->firstName = $each["FIRST_NAME"];
            $student->lastName = $each["LAST_NAME"];
            $student->gender = $each["GENDER"];
            $student->dateBirth = $each["DATE_BIRTH"];
            $class = new ClassModel();
            $student->class = $class->getById($each["ID_CLASS"])->idClass;
            $student->class = $class->getById($each["ID_CLASS"])->nameClass;
            array_push($array, $student);
        }
        return $array;
    }
    public function getById($id)
    {
        // mở kết nối
        $connect = $this->open();
        $sql = "SELECT * FROM `STUDENT1` WHERE ID_STUDENT=$id";
        $result = mysqli_query($connect, $sql);
        // đóng kết nối
        $this->close($connect);
        $item = mysqli_fetch_assoc($result);
        $student = new StudentModel();
        // item:trả về bản ghi
        $student->idStudent = $item["ID_STUDENT"];
        $student->firstName = $item["FIRST_NAME"];
        $student->lastName = $item["LAST_NAME"];
        $student->gender = $item["GENDER"];
        $student->dateBirth = $item["DATE_BIRTH"];
        $class = new ClassModel();
        $student->class = $class->getById($item["ID_CLASS"]);

        return $student;
    }

    public function update()
    {

        //mở kết nối
        $connect = $this->open();
        $sql = "UPDATE STUDENT1 SET FIRST_NAME='$this->firstName', LAST_NAME='$this->lastName', GENDER=$this->gender, DATE_BIRTH='$this->dateBirth', ID_CLASS=$this->class WHERE ID_STUDENT=$this->idStudent";
        mysqli_query($connect, $sql);
        
        // //đóng kết nối
        $this->close($connect);
        
    }


}